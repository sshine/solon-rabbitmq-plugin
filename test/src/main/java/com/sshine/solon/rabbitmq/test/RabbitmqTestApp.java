package com.sshine.solon.rabbitmq.test;

import org.noear.solon.Solon;

/**
 * @author noear 2021/1/27 created
 */
public class RabbitmqTestApp {
    public static void main(String[] args) {
        Solon.start(RabbitmqTestApp.class, args);
    }
}
