package com.sshine.solon.rabbitmq.test.controller.event;

import com.sshine.solon.rabbitmq.annotation.RabbitListener;
import com.sshine.solon.rabbitmq.core.Message;
import com.sshine.solon.rabbitmq.service.RabbitMessageHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author noear 2021/1/27 created
 */
@Slf4j
@RabbitListener(queues = "queue_one")
public class ExampleQueueTwo implements RabbitMessageHandler {
    @Override
    public boolean handle(Message message) throws Throwable {
        String body = new String(message.getBody());
        log.info(body);
        return true;
    }
}
