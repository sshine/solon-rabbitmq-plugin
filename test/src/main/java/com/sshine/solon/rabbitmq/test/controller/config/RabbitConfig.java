package com.sshine.solon.rabbitmq.test.controller.config;

import org.noear.solon.annotation.Configuration;

/**
 * @author sshine
 * @date 2022/7/25
 */
@Configuration
public class RabbitConfig {
    /*@Bean("exchange1")
    public Exchange exchange1() {
        return ExchangeBuilder.directExchange("exchange1").build();
    }

    @Bean("exchange2")
    public Exchange exchange2() {
        return ExchangeBuilder.directExchange("exchange2").build();
    }

    @Bean("dlx_exchange")
    public Exchange dlx_exchange() {
        return ExchangeBuilder.directExchange("dlx_exchange").build();
    }

    @Bean("queueOne")
    public Queue queueOne() {
        return QueueBuilder.durable("queue_one").build();
    }

    @Bean("queueTwo")
    public Queue queueTwo() {
        return QueueBuilder.durable("queue_two").build();
    }

    @Bean("queueThree")
    public Queue queueThree() {
        return QueueBuilder.durable("queue_three").build();
    }

    @Bean("queueFour")
    public Queue queueFour() {
        return QueueBuilder.durable("queue_four").build();
    }

    @Bean("queueFive")
    public Queue queueFive() {
        return QueueBuilder.durable("queue_five").deadLetterExchange("dlx_exchange").deadLetterRoutingKey("key_dlx").ttl(60000L).build();
    }

    @Bean("queueDlx")
    public Queue queueDlx() {
        return QueueBuilder.durable("queue_dlx").build();
    }

    @Bean("binding1")
    public Binding binding1(@Inject("queueOne") Queue queueOne, @Inject("exchange1") Exchange exchange1) {
        return BindingBuilder.bind(queueOne).to(exchange1).with("key_one").noargs();
    }

    @Bean("binding2")
    public Binding binding2(@Inject("queueTwo") Queue queueTwo,@Inject("exchange1") Exchange exchange1) {
        return BindingBuilder.bind(queueTwo).to(exchange1).with("key_two").noargs();
    }

    @Bean("binding3")
    public Binding binding3(@Inject("queueThree") Queue queueThree,@Inject("exchange2") Exchange exchange2) {
        return BindingBuilder.bind(queueThree).to(exchange2).with("key_one").noargs();
    }

    @Bean("binding4")
    public Binding binding4(@Inject("queueFour") Queue queueFour,@Inject("exchange2") Exchange exchange2) {
        return BindingBuilder.bind(queueFour).to(exchange2).with("key_two").noargs();
    }

    @Bean("binding5")
    public Binding binding5(@Inject("queueFive") Queue queueFive,@Inject("exchange2") Exchange exchange2) {
        return BindingBuilder.bind(queueFive).to(exchange2).with("key_three").noargs();
    }

    @Bean("binding_dlx")
    public Binding binding_dlx(@Inject("queueDlx") Queue queueDlx,@Inject("dlx_exchange") Exchange dlx_exchange) {
        return BindingBuilder.bind(queueDlx).to(dlx_exchange).with("key_dlx").noargs();
    }*/


}
