package com.sshine.solon.rabbitmq.test.controller;

import com.sshine.solon.rabbitmq.RabbitAdmin;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

/**
 * @author noear 2021/1/27 created
 */
@Controller
public class TestController {
    @Inject
    RabbitAdmin rabbitAdmin;

    @Get
    @Mapping("/test1")
    public Object test1() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_one.getExchange(),QueueEnum.queue_one.getRoutingKey(),"消息1");
        return true;
    }

    @Get
    @Mapping("/test2")
    public Object test2() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_two.getExchange(),QueueEnum.queue_two.getRoutingKey(),"消息2");
        return true;
    }

    @Get
    @Mapping("/test3")
    public Object test3() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_three.getExchange(),QueueEnum.queue_three.getRoutingKey(),"消息3");
        return true;
    }

    @Get
    @Mapping("/test4")
    public Object test4() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_four.getExchange(),QueueEnum.queue_four.getRoutingKey(),"消息4");
        return true;
    }

    @Get
    @Mapping("/test5")
    public Object test5() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_five.getExchange(),QueueEnum.queue_five.getRoutingKey(),"消息5");
        return true;
    }

    @Get
    @Mapping("/fanout")
    public Object fanout() throws Exception {
        rabbitAdmin.convertAndSend(QueueEnum.queue_fanout.getExchange(),QueueEnum.queue_fanout.getRoutingKey(),"广播消息");
        return true;
    }

}
