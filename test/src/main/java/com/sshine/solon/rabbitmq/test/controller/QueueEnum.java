package com.sshine.solon.rabbitmq.test.controller;

/**
 * @author sshine
 * @date 2022/7/25
 */
public enum QueueEnum {
    queue_one("exchange1","queue_one","key_one"),
    queue_two("exchange1","queue_two","key_two"),
    queue_three("exchange2","queue_three","key_one"),
    queue_four("exchange2","queue_four","key_two"),
    queue_five("exchange2","queue_five","key_three"),
    queue_fanout("fanout_exchange","","");

    private String exchange;

    private String queue;

    private String routingKey;

    QueueEnum(String exchange, String queue, String routingKey) {
        this.exchange = exchange;
        this.queue = queue;
        this.routingKey = routingKey;
    }

    public String getExchange() {
        return exchange;
    }

    public String getQueue() {
        return queue;
    }

    public String getRoutingKey() {
        return routingKey;
    }
}
