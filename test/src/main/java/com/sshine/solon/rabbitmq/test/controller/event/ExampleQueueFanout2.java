package com.sshine.solon.rabbitmq.test.controller.event;

import com.rabbitmq.client.BuiltinExchangeType;
import com.sshine.solon.rabbitmq.annotation.Exchange;
import com.sshine.solon.rabbitmq.annotation.Queue;
import com.sshine.solon.rabbitmq.annotation.QueueBinding;
import com.sshine.solon.rabbitmq.annotation.RabbitListener;
import com.sshine.solon.rabbitmq.core.Message;
import com.sshine.solon.rabbitmq.service.RabbitMessageHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author noear 2021/1/27 created
 */
@Slf4j
@RabbitListener(bindings = {
        @QueueBinding(exchange = @Exchange(name = "fanout_exchange",type = BuiltinExchangeType.FANOUT), queue = @Queue(name = "queue_fanout2"))
})
public class ExampleQueueFanout2 implements RabbitMessageHandler {
    @Override
    public boolean handle(Message message) throws Throwable {
        String body = new String(message.getBody());
        log.info(body);
        return true;
    }
}
