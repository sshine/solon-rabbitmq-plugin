package com.sshine.solon.rabbitmq.test.controller.event;

import com.sshine.solon.rabbitmq.core.Message;
import com.sshine.solon.rabbitmq.service.RabbitMessageHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author noear 2021/1/27 created
 */
@Slf4j
/*@RabbitListener(bindings = {
        @QueueBinding(exchange = @Exchange(name = "dlx_exchange"), queue = @Queue(name = "queue_dlx"), key = {"key_dlx","key_dlx2"}),

        @QueueBinding(exchange = @Exchange(name = "exchange2"),queue = @Queue(name = "queue_five",argument = {
                @Argument(name = "x-dead-letter-exchange",value = "dlx_exchange",type = String.class),
                @Argument(name = "x-message-ttl",value = "60000",type = long[].class),
                @Argument(name = "x-dead-letter-routing-key",value = "key_dlx2")
        }),key = "key_three")
})*/
public class ExampleQueueDlx implements RabbitMessageHandler {
    @Override
    public boolean handle(Message message) throws Throwable {
        String body = new String(message.getBody());
        log.info(body);
        return true;
    }
}
