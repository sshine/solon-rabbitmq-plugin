package com.sshine.solon.rabbitmq.test.controller;

import java.io.Serializable;

/**
 * @author sshine
 * @date 2022/7/25
 */
public class Student implements Serializable {
    private String name;
    private int age;

    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
