package com.sshine.solon.rabbitmq;

/**
 * @author sshine
 * @date 2022/7/25
 */
public class MessageConversionException extends RabbitmqException {
    public MessageConversionException(String message) {
        super(message);
    }

    public MessageConversionException(Throwable cause) {
        super(cause);
    }

    public MessageConversionException(String message, Throwable cause) {
        super(message, cause);
    }
}
