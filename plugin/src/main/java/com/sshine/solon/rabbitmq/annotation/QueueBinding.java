package com.sshine.solon.rabbitmq.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author sshine
 * @date 2022/7/21
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface QueueBinding {
    /**
     * @return the queue.
     */
    Queue queue();

    /**
     * @return the exchange.
     */
    Exchange exchange();

    /**
     * @return the routing key or pattern for the binding.
     * Multiple elements will result in multiple bindings.
     */
    String[] key() default {};

    /**
     * @return true if the declaration exceptions should be ignored.
     * @since 1.6
     */
    boolean ignoreDeclarationExceptions() default false;

    /**
     * @return true if the admin(s), if present, should declare this component.
     * @since 2.1
     */
    boolean declare() default true;

    Argument[] argument() default {};
}
