package com.sshine.solon.rabbitmq.service;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import com.sshine.solon.rabbitmq.core.MessageProperties;

/**
 * @author sshine
 * @date 2022/7/25
 */
public interface MessagePropertiesConverter {

    MessageProperties toMessageProperties(AMQP.BasicProperties source, Envelope envelope, String charset);

    AMQP.BasicProperties fromMessageProperties(MessageProperties source, String charset);
}
