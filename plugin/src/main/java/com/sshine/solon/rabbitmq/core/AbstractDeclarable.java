/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sshine.solon.rabbitmq.core;

import com.sshine.solon.rabbitmq.service.Declarable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Base class for {@link Declarable} classes.
 *
 * @author Gary Russell
 * @since 1.2
 */
public abstract class AbstractDeclarable implements Declarable {

    private boolean shouldDeclare = true;

    private boolean ignoreDeclarationExceptions;

    private final Map<String, Object> arguments;

    public AbstractDeclarable() {
        this(null);
    }

    /**
     * Construct an instance with the supplied arguments, or an empty map if null.
     *
     * @param arguments the arguments.
     * @since 2.2.2
     */
    public AbstractDeclarable(Map<String, Object> arguments) {
        if (arguments != null) {
            this.arguments = new ConcurrentHashMap<>(arguments);
        } else {
            this.arguments = new ConcurrentHashMap<String, Object>();
        }
    }

    @Override
    public boolean shouldDeclare() {
        return this.shouldDeclare;
    }

    /**
     * Whether or not this object should be automatically declared
     * by any {@code AmqpAdmin}. Default is {@code true}.
     *
     * @param shouldDeclare true or false.
     */
    public void setShouldDeclare(boolean shouldDeclare) {
        this.shouldDeclare = shouldDeclare;
    }

    @Override
    public void addArgument(String argName, Object argValue) {
        this.arguments.put(argName, argValue);
    }

    @Override
    public Object removeArgument(String name) {
        return this.arguments.remove(name);
    }

    public Map<String, Object> getArguments() {
        return this.arguments;
    }

    @Override
    public boolean isIgnoreDeclarationExceptions() {
        return this.ignoreDeclarationExceptions;
    }

    /**
     * Set to true to ignore exceptions such as mismatched properties when declaring.
     * @param ignoreDeclarationExceptions the ignoreDeclarationExceptions.
     * @since 1.6
     */
    public void setIgnoreDeclarationExceptions(boolean ignoreDeclarationExceptions) {
        this.ignoreDeclarationExceptions = ignoreDeclarationExceptions;
    }

}
