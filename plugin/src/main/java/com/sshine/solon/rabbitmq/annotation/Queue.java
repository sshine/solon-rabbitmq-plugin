package com.sshine.solon.rabbitmq.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author sshine
 * @date 2022/7/21
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface Queue {

    /**
     * @return the queue name or "" for a generated queue name (default).
     * @since 2.0
     */
    String name() default "";

    /**
     * Specifies if this queue should be durable.
     * By default if queue name is provided it is durable.
     * @return true if the queue is to be declared as durable.
     */
    boolean durable() default true;

    /**
     * Specifies if this queue should be exclusive.
     * By default if queue name is provided it is not exclusive.
     * @return true if the queue is to be declared as exclusive.
     */
    boolean exclusive() default false;

    /**
     * Specifies if this queue should be auto deleted when not used.
     * By default if queue name is provided it is not auto-deleted.
     * @return true if the queue is to be declared as auto-delete.
     */
    boolean autoDelete() default false;

    /**
     * @return true if the admin(s), if present, should declare this component.
     * @since 2.1
     */
    boolean declare() default true;

    /**
     * @return true if the declaration exceptions should be ignored.
     * @since 1.6
     */
    boolean ignoreDeclarationExceptions() default false;

    Argument[] argument() default {};


}
