package com.sshine.solon.rabbitmq.listener;

import com.sshine.solon.rabbitmq.RabbitAdmin;
import com.sshine.solon.rabbitmq.core.Queue;
import org.noear.solon.core.event.EventListener;

/**
 * @author sshine
 * @date 2022/7/21
 */
public class QueueEventListener implements EventListener<Queue> {

    @Override
    public void onEvent(Queue queue) throws Throwable {
        RabbitAdmin.addToContainer(queue);
    }
}
