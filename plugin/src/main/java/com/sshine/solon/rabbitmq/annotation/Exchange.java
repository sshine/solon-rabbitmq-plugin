package com.sshine.solon.rabbitmq.annotation;

import com.rabbitmq.client.BuiltinExchangeType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author sshine
 * @date 2022/7/21
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface Exchange {

    /**
     * @return the exchange name.
     */
    String name() default "";

    /**
     * The exchange type, including custom.
     * Defaults to {@link BuiltinExchangeType#DIRECT}.
     * If a custom exchange type is used the corresponding plugin is required on the broker.
     * @return the exchange type.
     * @see BuiltinExchangeType
     */
    BuiltinExchangeType type() default BuiltinExchangeType.DIRECT;

    /**
     * @return false if the exchange is to be declared as non-durable.
     */
    boolean durable() default true;

    /**
     * @return true if the exchange is to be declared as auto-delete.
     */
    boolean autoDelete() default false;

    /**
     * @return true if the exchange is to be declared as internal.
     * @since 1.6
     */
    boolean internal() default false;

    /**
     * @return true if the exchange is to be declared as an
     * 'x-delayed-message' exchange. Requires the delayed message exchange
     * plugin on the broker.
     * @since 1.6.4
     */
    boolean delayed() default false;

    /**
     * @return true if the admin(s), if present, should declare this component.
     * @since 2.1
     */
    boolean declare() default true;

    /**
     * @return true if the declaration exceptions should be ignored.
     * @since 1.6
     */
    boolean ignoreDeclarationExceptions() default false;

    Argument[] argument() default {};

}
