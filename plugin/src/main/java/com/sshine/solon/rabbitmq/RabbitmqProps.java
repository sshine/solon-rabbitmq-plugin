package com.sshine.solon.rabbitmq;

import org.noear.solon.Solon;

/**
 * @author noear
 * @since 1.2
 */
public class RabbitmqProps {
    public static final String VIRTUALHOST = "solon.rabbitmq.virtualHost";
    public static final String USERNAME = "solon.rabbitmq.username";
    public static final String PASSWORD = "solon.rabbitmq.password";
    public static final String RABBITMQ_ENABLE = "solon.rabbitmq.enable";
    public static final String RABBITMQ_SERVER = "solon.rabbitmq.server";
    public static final String RABBITMQ_PREFETCH_COUNT = "solon.rabbitmq.prefetchCount";
    public static final String RABBITMQ_TIMEOUT = "solon.rabbitmq.timeout";

    public static String getVirtualhost() {
        return Solon.cfg().get(VIRTUALHOST);
    }

    public static String getUsername() {
        return Solon.cfg().get(USERNAME);
    }

    public static String getPassword() {
        return Solon.cfg().get(PASSWORD);
    }

    public static boolean getRabbitmqEnable() {
        return Solon.cfg().getBool(RABBITMQ_ENABLE, true);
    }

    public static String getRabbitmqServer() {
        return Solon.cfg().get(RABBITMQ_SERVER);
    }

    public static int getRabbitmqPrefetchCount() {
        return Solon.cfg().getInt(RABBITMQ_PREFETCH_COUNT, 1);
    }

    public static long getRabbitmqTimeout() {
        return Solon.cfg().getLong(RABBITMQ_TIMEOUT, 0L);
    }
}
