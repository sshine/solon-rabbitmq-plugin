/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sshine.solon.rabbitmq.service;

/**
 * Classes implementing this interface can be auto-declared
 * with the broker during context initialization by an {@code AmqpAdmin}.
 * Registration can be limited to specific {@code AmqpAdmin}s.
 *
 * @author Gary Russell
 * @since 1.2
 *
 */
public interface Declarable {

	/**
	 * Whether or not this object should be automatically declared
	 * by any {@code AmqpAdmin}.
	 * @return true if the object should be declared.
	 */
	boolean shouldDeclare();

	/**
	 * Add an argument to the declarable.
	 * @param name the argument name.
	 * @param value the argument value.
	 * @since 2.2.2
	 */
	default void addArgument(String name, Object value) {
		// default no-op
	}

	/**
	 * Remove an argument from the declarable.
	 * @param name the argument name.
	 * @return the argument value or null if not present.
	 * @since 2.2.2
	 */
	default Object removeArgument(String name) {
		return null;
	}


	/**
	 * Should ignore exceptions (such as mismatched args) when declaring.
	 * @return true if should ignore.
	 * @since 1.6
	 */
	boolean isIgnoreDeclarationExceptions();

}
