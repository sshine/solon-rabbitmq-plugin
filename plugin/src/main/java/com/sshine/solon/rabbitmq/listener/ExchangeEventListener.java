package com.sshine.solon.rabbitmq.listener;

import com.sshine.solon.rabbitmq.RabbitAdmin;
import com.sshine.solon.rabbitmq.service.Exchange;
import org.noear.solon.core.event.EventListener;

/**
 * @author sshine
 * @date 2022/7/21
 */
public class ExchangeEventListener implements EventListener<Exchange> {

    @Override
    public void onEvent(Exchange exchange) throws Throwable {
        RabbitAdmin.addToContainer(exchange);
    }
}
