package com.sshine.solon.rabbitmq;

import com.sshine.solon.rabbitmq.annotation.RabbitListener;
import com.sshine.solon.rabbitmq.builder.RabbitListenerBeanBuilder;
import com.sshine.solon.rabbitmq.core.Binding;
import com.sshine.solon.rabbitmq.core.Queue;
import com.sshine.solon.rabbitmq.listener.BindingEventListener;
import com.sshine.solon.rabbitmq.listener.ExchangeEventListener;
import com.sshine.solon.rabbitmq.listener.QueueEventListener;
import com.sshine.solon.rabbitmq.service.Exchange;
import org.noear.solon.Utils;
import org.noear.solon.core.AopContext;
import org.noear.solon.core.Plugin;
import org.noear.solon.core.event.EventBus;

/**
 * @author noear
 * @since 1.2
 */
public class XPluginImp implements Plugin {
    @Override
    public void start(AopContext context) {
        if (Utils.isEmpty(RabbitmqProps.getRabbitmqServer())) {
            return;
        }
        if (RabbitmqProps.getRabbitmqEnable()) {
            context.beanBuilderAdd(RabbitListener.class,context.getBeanOrNew(RabbitListenerBeanBuilder.class));
            EventBus.subscribe(Exchange.class,context.getBeanOrNew(ExchangeEventListener.class));
            EventBus.subscribe(Queue.class,context.getBeanOrNew(QueueEventListener.class));
            EventBus.subscribe(Binding.class,context.getBeanOrNew(BindingEventListener.class));
            context.beanOnloaded(aopContext -> {
                RabbitAdmin rabbitAdmin = aopContext.getBeanOrNew(RabbitAdmin.class);
                rabbitAdmin.init();
            });
        }
    }
}
