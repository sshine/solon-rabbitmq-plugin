package com.sshine.solon.rabbitmq.service;

import com.sshine.solon.rabbitmq.MessageConversionException;
import com.sshine.solon.rabbitmq.core.Message;
import com.sshine.solon.rabbitmq.core.MessageProperties;

/**
 * @author sshine
 * @date 2022/7/25
 */
public interface MessageConvert {

    /**
     * Convert a Java object to a Message.
     * @param object the object to convert
     * @param messageProperties The message properties.
     * @return the Message
     * @throws MessageConversionException in case of conversion failure
     */
    Message toMessage(Object object, MessageProperties messageProperties) throws MessageConversionException;

    /**
     * Convert from a Message to a Java object.
     * @param message the message to convert
     * @return the converted Java object
     * @throws MessageConversionException in case of conversion failure
     */
    Object fromMessage(Message message) throws MessageConversionException;
}
