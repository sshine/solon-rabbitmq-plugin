package com.sshine.solon.rabbitmq.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author sshine
 * @date 2022/7/21
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface Argument {

    /**
     * 参数名
     * @return 参数名
     */
    String name();

    /**
     * The argument value, an empty string is translated to {@code null} for example
     * to represent a present header test for a headers exchange.
     * @return the argument value.
     */
    String value() default "";

    /**
     * Return the argument value.
     *
     * @return the type of the argument value.
     */
    Class type() default String.class;
}
