package com.sshine.solon.rabbitmq.listener;

import com.sshine.solon.rabbitmq.RabbitAdmin;
import com.sshine.solon.rabbitmq.core.Binding;
import org.noear.solon.core.event.EventListener;

/**
 * @author sshine
 * @date 2022/7/21
 */
public class BindingEventListener implements EventListener<Binding> {

    @Override
    public void onEvent(Binding binding) throws Throwable {
        RabbitAdmin.addToContainer(binding);
    }
}
