package com.sshine.solon.rabbitmq.service;

import com.sshine.solon.rabbitmq.core.Message;

/**
 * @author sshine
 * @date 2022/7/20
 */
@FunctionalInterface
public interface RabbitMessageHandler {

    /**
     * 处理
     *
     * @param message 消息
     * */
    boolean handle(Message message) throws Throwable;
}
